# README #

A Gradle plugin for adding Animal Sniffer support. Animal Sniffer checks code against a signature jar in order to verify that no invalid API's are being used. Most common use case is to check whether no new JDK API is being used when compiling to older JDK targets.

## Usage ##

```
#!groovy

buildscript {
    repositories {
        jcenter()
    }
    dependencies {
        classpath "be.insaneprogramming.gradle:animalsniffer-gradle-plugin:+"
    }
}
apply plugin: 'be.insaneprogramming.gradle.animalsniffer'
animalsniffer {
    signature = "org.codehaus.mojo.signature:java16:+@signature"
}
```

The following plugin settings are supported:

```
#!groovy

animalsniffer {
    // either use signature or signatures, not both
    signature = "org.codehaus.mojo.signature:java16:+@signature"
    signatures = ["org.codehaus.mojo.signature:java16:+@signature"] // support for multiple signatures
    ignores = ["my.package.name"]
    annotations = [] // supported annotations to signal Animal Sniffer to ignore annotated elements
    excludeDependencies = true // only disable if you have signatures for all dependencies!
    skip = false // skip animal sniffer tasks
    runForTests = true // run animal sniffer for test classes as well
}
```

## Possible signatures ##

Most signatures can be found on Maven Central: http://search.maven.org/#search%7Cga%7C2%7Csignature

## Gradle plugin DSL ##

